using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.App.Utils.Client;
using Otus.Teaching.Pcf.App.Utils.Settings;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.App.Pages.Administration.Customers
{
    public class IndexModel : PageModel
    {
        private AppSettings AppSettings { get; }
        private HttpClient HttpClient { get; }

        public IndexModel(IOptions<AppSettings> appSettings)
        {
            HttpClient = new HttpClient();
            this.AppSettings = appSettings.Value;

            HttpClient.BaseAddress = new Uri(this.AppSettings.AddressSeverCustomer);
            HttpClient.DefaultRequestHeaders.Add("User-Agent", "Server");
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        public async Task<List<CustomerShortResponse>> GetCustomerAllAsync()
        {
            return await new WebClient<CustomerShortResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverCustomer + "Customers");
        }
       
        public async Task<CreateOrEditCustomerRequest> GetCustomerAsync(string Id) 
        {
            return await new WebClient<CreateOrEditCustomerRequest>(HttpClient).GetCustomerAsync(Guid.Parse(Id), AppSettings.AddressSeverCustomer + "Customers");
        }

        public  List<PreferenceResponse> GetResponseAllAsync()
        {
            return new WebClient<PreferenceResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverCustomer + "Preferences").Result.ToList<PreferenceResponse>();
        }

        public async Task<IActionResult> OnPostSubmit(CreateOrEditCustomerRequest customerRequest)
        {
            await new WebClient<CreateOrEditCustomerRequest>(HttpClient).Create(customerRequest, AppSettings.AddressSeverCustomer + "Customers");

            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostDeleteAsync(Guid Id)
        {
            await new WebClient<Guid>(HttpClient).Delete(Id, AppSettings.AddressSeverCustomer + "Delete");

            return RedirectToPage();
        }

        public JsonResult OnPost(string Id)
        {
            var Customer = new WebClient<CustomerResponse>(HttpClient).GetCustomerAsync(Guid.Parse(Id), AppSettings.AddressSeverCustomer + "Customers").Result;
            
            string json = Newtonsoft.Json.JsonConvert.SerializeObject(Customer);

            return new JsonResult(json);
        }

        public async  Task<IActionResult> OnPostEdit(Guid Id, CreateOrEditCustomerRequest customerResponse)
        {
            await new  WebClient<CreateOrEditCustomerRequest>(HttpClient).UpdateAsync(Id, customerResponse, AppSettings.AddressSeverCustomer + "Customers");

            return RedirectToPage();
        }
    }
}