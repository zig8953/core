using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.App.Utils.Client;
using Otus.Teaching.Pcf.App.Utils.Settings;
using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;

namespace Otus.Teaching.Pcf.App.Pages.Administration
{
    public class PreferenceModel : PageModel
    {
        private AppSettings AppSettings { get; }
        private HttpClient HttpClient { get; }

        public PreferenceModel(IOptions<AppSettings> appSettings)
        {
            HttpClient = new HttpClient();
            this.AppSettings = appSettings.Value;

            HttpClient.BaseAddress = new Uri(this.AppSettings.AddressSever);
            HttpClient.DefaultRequestHeaders.Add("User-Agent", "Server");
        }

        public void OnGet()
        {
        }

        public async Task<List<PreferenceResponse>> GetAllAsync()
        {
            return await new WebClient<PreferenceResponse>(HttpClient).GetAllAsync(AppSettings.AddressSever + "Preferences");
        }
    }
}
