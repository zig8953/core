using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.Administration.WebHost.Models;
using Otus.Teaching.Pcf.App.Utils.Client;
using Otus.Teaching.Pcf.App.Utils.Settings;
using Otus.Teaching.Pcf.ReceivingFromPartner.WebHost.Models;

namespace Otus.Teaching.Pcf.App.Pages.Administration.promocodes
{
    public class IndexModel : PageModel
    {
        private AppSettings AppSettings { get; }
        private HttpClient HttpClient { get; }

        public IndexModel(IOptions<AppSettings> appSettings)
        {
            HttpClient = new HttpClient();
            this.AppSettings = appSettings.Value;

            HttpClient.BaseAddress = new Uri(this.AppSettings.AddressSeverPartner);
            HttpClient.DefaultRequestHeaders.Add("User-Agent", "Server");
        }
        public void OnGet()
        {
        }

        public List<PreferenceResponse> GetResponseAllAsync()
        {
            return new WebClient<PreferenceResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverPartner + "Partners").Result.ToList<PreferenceResponse>()
;
        }   
        
        public async Task<List<PreferenceResponse>> GetPreferenceIdAllAsync()
        {
            return await new WebClient<PreferenceResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverCustomer + "Preferences");
        }

        public List<EmployeeShortResponse> GetEmployeesAllAsync()
        {
            return new WebClient<EmployeeShortResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverAdministration + "Employees").Result.ToList<EmployeeShortResponse>();
        }

        public async void OnPostSubmit(Guid Id, ReceivingPromoCodeRequest request)
        {
            await new WebClient<ReceivingPromoCodeRequest>(HttpClient).CreatePromo(Id, request, AppSettings.AddressSeverPartner + "Partners");
        }

        public  List<PromoCodeShortResponse> GetAllPromocodesAsync()
        {
            return  new WebClient<PromoCodeShortResponse>(HttpClient).GetAllAsync(AppSettings.AddressSeverCustomer + "Promocodes").Result.ToList<PromoCodeShortResponse>();
        }
    }
}
