﻿using Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.App.Service
{
    interface IPreferenceService
    {
        Task<List<PreferenceResponse>> GetAllAsync();
    }
}
