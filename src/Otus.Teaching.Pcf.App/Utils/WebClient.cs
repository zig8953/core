﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.App.Utils.Client
{
    public class WebClient<T>
    {
        private readonly HttpClient _httpClient;
        
        public WebClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Guid> Create(T obj, string requestUri)
        {
            string serializedUser = JsonConvert.SerializeObject(obj);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, requestUri)
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await _httpClient.SendAsync(requestMessage);
            _ = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<Guid>(responseBody);

            return returnedUser;
        }

        public async Task<Guid> CreatePromo(Guid Id, T obj, string requestUri)
        {
            if (Id == default)
                throw new System.ArgumentNullException("id");

            if (obj == null)
                throw new System.ArgumentNullException("obj");

            string serializedUser = JsonConvert.SerializeObject(obj);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{requestUri}/{Id}")
            {
                Content = new StringContent(serializedUser)
            };

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response = await _httpClient.SendAsync(requestMessage);
            _ = response.StatusCode;
            var responseBody = await response.Content.ReadAsStringAsync();

            var returnedUser = JsonConvert.DeserializeObject<Guid>(responseBody);

            return returnedUser;
        }

        public async Task<List<T>> GetAllAsync(string addressSever)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, addressSever);

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return await Task.FromResult(JsonConvert.DeserializeObject<List<T>>(responseBody));
            }
            else
                return null;
        }

        public async Task<T> GetCustomerAsync(Guid Id, string addressSever)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, $"{addressSever}/{Id}");

            var response = await _httpClient.SendAsync(requestMessage);

            var responseStatusCode = response.StatusCode;

            if (responseStatusCode.ToString() == "OK")
            {
                var responseBody = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<T>(responseBody);
            }
            else
            {
                throw new ($"Код состояния HTTP ответа не ожидался: {responseStatusCode} ");
            }
        }

        public Task<T> Delete(T obj, string addressSever)
        {
            string serializedUser = JsonConvert.SerializeObject(obj);

            var requestMessage = new HttpRequestMessage(HttpMethod.Post, addressSever);
            requestMessage.Content = new StringContent(serializedUser);

            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response =  _httpClient.SendAsync(requestMessage);

            return Task.FromResult(obj);
        }

        public async Task<string> UpdateAsync(Guid Id, T obj, string addressSever)
        {
            if (Id == default)
                throw new System.ArgumentNullException("id");

            if (obj == null)
                throw new System.ArgumentNullException("obj");

            string serializedUser = JsonConvert.SerializeObject(obj);
        
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, $"{addressSever}/{Id}");

            requestMessage.Content = new StringContent(serializedUser);
            requestMessage.Content.Headers.ContentType
                = new System.Net.Http.Headers.MediaTypeHeaderValue("application/json");

            var response =  await _httpClient.SendAsync(requestMessage);

            return response.ToString();

        }

    }
}
