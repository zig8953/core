﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.App.Utils.Settings
{
    public class AppSettings
    {
        public string AddressSeverCustomer { get; set; }
        public string AddressSeverPartner { get; set; }
        public string AddressSeverAdministration { get; set; }
    }
}
