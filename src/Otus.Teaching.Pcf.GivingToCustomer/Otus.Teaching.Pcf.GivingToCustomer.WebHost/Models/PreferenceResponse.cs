﻿using System;
using System.ComponentModel;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Models
{
    public class PreferenceResponse
    {
        public Guid Id { get; set; }

        [DisplayName("Northwind Products")]
        public string Name { get; set; }
    }
}